//
//  ItemManager.swift
//  ToDo
//
//  Created by PAOLA GUAMANI on 9/5/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import Foundation
import RealmSwift

class ItemManager {
    
    var toDoItems:[Item] = []
    var donItems:[Item] = []
    var realm: Realm
    
    // instancia de Realm
    // para que se comparta en toda la app
    init(){
        realm = try! Realm()
        print(realm.configuration.fileURL)
        
        // ver archivos ocultos shift + command + .
    }
    
    //pasar cosas de to a Done
    func checkItem(index: Int){
//        let item = toDoItems.remove(at: index)
//        donItems += [item]
        
        // obtengo el item del arreglo para actualizar prop
        let item = toDoItems[index]
        
        // actualizo una propiedad del objeto en la base
        try! realm.write {
            item.done = true
        }
    }
    
    func unCheckItem(index: Int){
//        let item = donItems.remove(at: index)
//        toDoItems += [item]
        let item = donItems[index]
        
        try! realm.write {
            item.done = false
        }
    }
    
    func addItem(title: String, location:String, itemDescription: String?){
        let item = Item()
        item.id = "\(UUID())"
        item.title = title
        item.location = location
        item.itemDescription = itemDescription
        
        // guardo el item en la base
        try! realm.write {
            realm.add(item)
        }
        
        // getTodoItems()
    }
    
    // actualizo al mismo tiempo las dos listas
    func upDateArrays(){
        toDoItems = Array(realm.objects(Item.self).filter("done = false"))
        donItems = Array(realm.objects(Item.self).filter("done = true"))
    }
    
    // guardo en el Arreglo lo que To Do items
    func getTodoItems(){
        toDoItems = Array(realm.objects(Item.self).filter("done = false"))
    }
    
    // los Done
    func getDoneItems(){
        donItems = Array(realm.objects(Item.self).filter("done = true"))
    }
}
