//
//  Item.swift
//  ToDo
//
//  Created by PAOLA GUAMANI on 9/5/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import Foundation
import RealmSwift

// defino los modelos como clase regular
class Item: Object{
    @objc dynamic var id: String?
    @objc dynamic var title: String?
    @objc dynamic var location: String?
    @objc dynamic var itemDescription: String?
    @objc dynamic var done = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

//struct Item{
//
//    let title: String
//    let location : String
//    let description : String
//
//}
